A blacklist against advertising tracking and malware to improve accessibility and security on the Internet. 
This list is recommended for the following services: pfSense NextDNS Pi-Hole

All contributions to this project are very welcome.
